import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app';
import Layout from './components/layout';
import Register from './components/users/register';
import UserList from './components/users/list';
import UserEdit from './components/users/edit';
import AddCategoryForm from './components/categories/add';

const Greeting = () => {
	return <div>Hey i am in Greeting</div>;
}


 
export default (
	<Route path="/" component={Layout}>
		<IndexRoute component={App} />
		<Route path="/greet" component={Greeting} />
		<Route path="/register" component={Register} />
		<Route path="/listing" component={UserList} />
		<Route path="/user/edit/" component={UserEdit} />
		<Route path="/category/add" component={AddCategoryForm} />
	</Route>
);