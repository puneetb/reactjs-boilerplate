import ACTION_TYPES from '../actions/action_types';
const INITIAL_STATE = {serverMessage:''}

const LoginReducer = (state = INITIAL_STATE,action) => {
	// console.log('TYPE==='+INITIAL_STATE);
	switch (action.type) {
		case ACTION_TYPES.EMAIL_CHANGED:
			// state.email = action.payload;
			// return state
			break;
		case ACTION_TYPES.SHOW_LOADER:
			return Object.assign({}, state, { showLoader: true });
		case ACTION_TYPES.LOGIN_USER:
			console.log(action.payload);
			//return state; 
			return Object.assign({}, state, { serverMessage: action.payload.serverMessage, showLoader: false });
		case ACTION_TYPES.ERROR_FETCHING:
			return Object.assign({}, state, { serverMessage: "Error connecting server", errorStatus:1 });
		case ACTION_TYPES.FETCH_USERS:
			return Object.assign({}, state, {errorStatus:0, userData: action.payload,showUserData: true,serverMessage:'',showLoader:false});
		case ACTION_TYPES.DELETE_USER:
			return Object.assign({}, state, { serverMessage: "User deleted Successfully", errorStatus:0, showUserData: true});
		case ACTION_TYPES.USER_DETAILS:
			return Object.assign({}, state, { serverMessage: "", errorStatus:0, showUserData: true, userDetails: action.payload});
		case ACTION_TYPES.CHECK_EMAIL:
			return Object.assign({}, state, { emailErrorMessage: action.payload.serverMessage });
		default:
			return state;
	}
};

export default LoginReducer;
