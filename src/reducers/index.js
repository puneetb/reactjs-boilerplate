import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form'
import LoginReducer from './LoginReducer';
import CategoryReducer from './CategoryReducer';

// const rootReducer = combineReducers({
//   state: (state = {}) => state
// });

const rootReducer = combineReducers({
  form: reduxFormReducer, // mounted under "form",
  login: LoginReducer,
  category: CategoryReducer
})

export default rootReducer;
