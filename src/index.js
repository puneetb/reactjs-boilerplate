import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import reducer from './reducers/index';
import routes from './routes'
import { Router, browserHistory } from 'react-router';

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

var injectTapEventPlugin = require("react-tap-event-plugin");

const store =
  //(window.devToolsExtension ? window.devToolsExtension()(createStore) : createStore)(reducer)
  applyMiddleware(thunk)(createStore)(reducer);

injectTapEventPlugin();


ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <Router history={browserHistory} routes={routes}/>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('content')
)

