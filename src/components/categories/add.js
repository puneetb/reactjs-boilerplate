import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { createCategory,fetchCategories } from './../../actions/CategoryActions';
import {connect} from 'react-redux';
import renderField from './../common/renderField';
import renderSelectField from './../common/renderSelectField';
import Loader from './../loader';


const validate = values => {
  const errors = {}
  if (!values.name) {
    errors.name = 'Please enter category name.'
  }
  
  if (!values.slug) {
    errors.slug = 'Please enter category slug'
  }
  

  return errors
}


class AddCategoryForm extends Component{

  constructor(props) {
    super(props);
    this.state = {
      serverMessageTest:'intialVals',
      showLoader: false,
      emailErrorMessage:'',
      categories: []
    }
  }

  componentWillMount() {
    this.props.dispatch(fetchCategories());
  }

  componentWillReceiveProps(nextProps) {
      
    this.setState({categories: nextProps.categories});
  }

  onSubmitubmitHandler(formProps) {
    console.log(formProps);
    //this.props.dispatch(registerUser(formProps));
  }



  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props
    return(

      <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
        {
          this.props.showLoader ? <Loader /> : ""
        }
        <Field name="name" type="text" component={renderField} label="Name" plaeholder="Name"/>
        <Field name="slug" type="text" component={renderField} label="slug" plaeholder="Slug"/>
        <Field name="parent_id" emptyValue="Parent Category" type="select" templateList={this.state.categories} component={renderSelectField} label="Category" plaeholder="Category"/>
        <div>
          <button type="submit" disabled={submitting}>Submit</button>
          <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</button>
        </div>
        {this.props.serverMessage}
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    serverMessageTest: state.category.serverMessageTest,
    categories: state.category.categories
  }
}

// export default reduxForm({
//   form: 'syncValidation1',  // a unique identifier for this form
//   validate,                // 
//   warn                     //
// },null,{loginUser})(SyncValidationForm)

AddCategoryForm = reduxForm({
  form: 'syncValidation2',  // a unique identifier for this form
  validate
})(AddCategoryForm)

export default connect(mapStateToProps, {createCategory,fetchCategories})(AddCategoryForm);


