import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { getUserDetails, updateUserData } from './../../actions/loginAction';
import {connect} from 'react-redux';
import renderField from './../common/renderField';
import { browserHistory } from 'react-router';
import Dropzone from 'react-dropzone';
import Alert from 'react-s-alert';

const validate = values => {
  const errors = {}
  if (!values.first_name) {
    errors.first_name = 'Please enter first name.'
  }
  if (!values.email) {
    errors.email = 'Please enter email.'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.last_name) {
    errors.last_name = 'Please enter last name'
  }
  if (values.password) {
    if (!values.cpassword) {
      errors.cpassword = 'Please enter confirm password'
    } else if(values.cpassword!==values.password) {
      errors.cpassword = 'Password and confirm password does not match'
    }
  }
  // if (!values.cpassword) {
  //   errors.cpassword = 'Please enter confirm password'
  // } else if(values.cpassword!==values.password) {
  //   errors.cpassword = 'Password and confirm password does not match'
  // }
  return errors
}

const warn = values => {
  const warnings = {}
  if (values.age < 19) {
    warnings.age = 'Hmm, you seem a bit young...'
  }
  return warnings
}



class SyncValidationForm extends Component{

  constructor(props) {
    super(props);
    this.state = {
      serverMessage:'',
      userData:null,
      emailD:'',
      initialValues:{},
      user_id:null,
      maxSize:2000000
    }
  }
  onSubmitubmitHandler(formProps) {
    formProps.imageBase64 = this.state.base64Image;
    this.props.dispatch(updateUserData(this.state.user_id, formProps));
    browserHistory.push('/listing')
  }

  componentWillMount() {
  	let user_id = this.props.location.query.user_id;
    this.setState({user_id: user_id});
  	this.props.dispatch(getUserDetails(user_id));
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
  }

  redirect_listing() {
    browserHistory.push('/listing');
  }

  imageExists(url, callback) {
    var img = new Image();
    img.onload = function() { callback(true); };
    img.onerror = function() { callback(false); };
    img.src = url;
  }

  /*Handle image drop*/
    onImageDrop(files) {
      this.setState({uploadedFile: null,base64Image: null,showBtn:false});
      if (files.length === 0) {
          // alert('Only gif, png, bmp, jpeg, jpg are allow.');
          Alert.error('Uploaded file is not a valid image. Only JPG, JPEG, PNG, BMP and GIF files are allowed.', {
          position: 'top-right'
          });
          return;
      }
      let file = files[0];
       console.log(file.size);
      if (file.size > this.state.maxSize) {
          let error_msg = `The file (${file.name}) size exceeds the limit allowed and cannot be saved.`;
          // alert(`File: ${file.name} size: ${file.size} > max: ${this.maxSize} bytes`);
          Alert.error(error_msg, {
          position: 'top-right'
          });
          return;
      }


      this.setState({uploadedFile: file});
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function(event) {  
        this.setState({base64Image: event.target.result});
      }.bind(this);
    }

  render() {
    console.log("inside render");
    const { handleSubmit, submitting } = this.props
    return(
      <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
        <Alert stack={{limit: 3}} />
        <Field name="first_name" type="text" component={renderField} label="First name" plaeholder="First Name" />
        <Field name="last_name" type="text" component={renderField} label="Last name" plaeholder="First Name"/>
        <Field name="email" type="text" component={renderField} label="Email" plaeholder="user@example.com"/>
        <Field name="password" type="password" component={renderField} label="password" plaeholder="Password"/>
        <Field name="cpassword" type="password" component={renderField} label="Confirm password" plaeholder="Confirm Password"/>
        <Dropzone
          multiple={false}
          accept="image/*"
          className="dropzone"
          onDrop={this.onImageDrop.bind(this)}>
          <p>Drop an image or click to select a file to upload max 2MB.</p>
        </Dropzone>
        <div>
          <button type="submit" disabled={submitting}>Submit</button>
          <button type="button" onClick={this.redirect_listing}>Cancel</button>
        </div>
        {this.props.serverMessage}
      </form>
    );
  }
}

function mapStateToProps(state) {


  return {
    erroStatus: state.login.erroStatus,
    serverMessage: state.login.serverMessage,
    userData: state.login.userData,
    initialValues: state.login.userDetails
  }
}

// export default reduxForm({
//   form: 'syncValidation1',  // a unique identifier for this form
//   validate,                // <--- validation function given to redux-form
//   warn                     // <--- warning function given to redux-form
// },null,{loginUser})(SyncValidationForm)

SyncValidationForm = reduxForm({
  form: 'syncValidation1',  // a unique identifier for this form
  enableReinitialize: true,
  validate,                // <--- validation function given to redux-form
  warn                     // <--- warning function given to redux-form
})(SyncValidationForm)

export default connect(mapStateToProps, {getUserDetails, updateUserData})(SyncValidationForm);


