import React, { Component } from 'react'
import { reduxForm } from 'redux-form'
import { fetchUserList,deleteUser } from './../../actions/loginAction';
import {connect} from 'react-redux';
import { Link } from 'react-router';



class SyncValidationForm extends Component{

  constructor(props) {
    super(props);
    this.state = {
      serverMessage:'',
      errorStatus:'',
      userData:"",
      showUserData:false
    }
  }
  componentWillMount() {
    this.getUsersData();
  }

  componentWillReceiveProps(nextProps) {
    //this.getUsersData();

  }

  getUsersData() {
    this.props.dispatch(fetchUserList());
  }

  componentDidUpdate() {
    this.getUsersData();
  }

  remove(user){
    this.props.dispatch(deleteUser(user._id));
    //console.log(this.props.userData);
  }

  renderUsers() {
    if(this.props.showUserData) {
     return this.props.userData.map((user) => {
        var style = {
          float: 'left',
          width: 250
        };
        var listStyle={
          width: '100%',
          float:'left'
        }
        return (
          <li  key={user._id} style={listStyle}>
              <div  style={style}>
                <img src={user.profile_image}  width="200" height="200" />
              </div>
              <div>{user.first_name}</div>
              <Link to={"/user/edit/?user_id="+ user._id}>
                  <span>{user.email}</span>
              </Link> &nbsp;&nbsp;&nbsp;
              <Link to={"/user/edit/?user_id="+ user._id}>
                  <span>Edit</span>
              </Link>
               &nbsp;&nbsp;&nbsp;
               <a href="#" data-id="{user._id}" className="remove-filter" onClick={this.remove.bind(this, user)}>remove</a>

           </li>
        );
         
     });
    }
  }

  render() {
    //console.log("render called" + this.props.showUserData + "==" + this.props.serverMessage);
    
    //const { handleSubmit, pristine, reset, submitting } = this.props
    return(
      <ul>
        {this.props.serverMessage ? this.props.serverMessage : ""}

        {this.renderUsers()}
      </ul>
    );
  }
}



function mapStateToProps(state) {
  return {
    erroStatus: state.login.erroStatus,
    serverMessage: state.login.serverMessage,
    errorStatus:state.login.errorStatus,
    userData: state.login.userData,
    showUserData: state.login.showUserData
  }
}

SyncValidationForm = reduxForm({
  form: 'syncValidation1' // <--- warning function given to redux-form
})(SyncValidationForm)

export default connect(mapStateToProps, {fetchUserList, deleteUser})(SyncValidationForm);


