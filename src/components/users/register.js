import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { registerUser,validateEmailExists } from './../../actions/loginAction';
import {connect} from 'react-redux';
import renderField from './../common/renderField';
import Loader from './../loader';


const validate = values => {
  const errors = {}
  if (!values.first_name) {
    errors.first_name = 'Please enter first name.'
  }
  if (!values.email) {
    errors.email = 'Please enter email.'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.last_name) {
    errors.last_name = 'Please enter last name'
  }
  if (!values.password) {
    errors.password = 'Please enter password'
  }
  if (!values.cpassword) {
    errors.cpassword = 'Please enter confirm password'
  } else if(values.cpassword!==values.password) {
    errors.cpassword = 'Password and confirm password does not match'
  }
  return errors
}

const warn = values => {
  const warnings = {}
  if (values.age < 19) {
    warnings.age = 'Hmm, you seem a bit young...'
  }
  return warnings
}


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const asyncValidate = (values, dispatch ) => {
  return sleep(1) // simulate server latency
    .then(() => {
      dispatch(validateEmailExists(values.email));      
    })
}


class SyncValidationForm extends Component{

  constructor(props) {
    super(props);
    this.state = {
      serverMessage:'',
      showLoader: false,
      emailErrorMessage:''
    }
  }
  onSubmitubmitHandler(formProps) {
    this.props.dispatch(registerUser(formProps));
  }



  render() {
    console.log(this.props.showLoader);
    const { handleSubmit, pristine, reset, submitting } = this.props
    return(

      <form onSubmit={handleSubmit(this.onSubmitubmitHandler.bind(this))}>
        {
          this.props.showLoader ? <Loader /> : ""
        }
        <Field name="first_name" type="text" component={renderField} label="First name" plaeholder="First Name"/>
        <Field name="last_name" type="text" component={renderField} label="Last name" plaeholder="First Name"/>
        <Field 
          name="email" 
          type="text" 
          component={renderField} 
          label="Email" 
          plaeholder="user@example.com"
        />
        {this.props.emailErrorMessage}
        <Field name="password" type="password" component={renderField} label="password" plaeholder="Password"/>
        <Field name="cpassword" type="password" component={renderField} label="Confirm password" plaeholder="Confirm Password"/>
        <div>
          <button type="submit" disabled={submitting}>Submit</button>
          <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</button>
        </div>
        {this.props.serverMessage}
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    erroStatus: state.login.erroStatus,
    serverMessage: state.login.serverMessage,
    showLoader: state.login.showLoader,
    emailErrorMessage: state.login.emailErrorMessage
  }
}

// export default reduxForm({
//   form: 'syncValidation1',  // a unique identifier for this form
//   validate,                // <--- validation function given to redux-form
//   warn                     // <--- warning function given to redux-form
// },null,{loginUser})(SyncValidationForm)

SyncValidationForm = reduxForm({
  form: 'syncValidation1',  // a unique identifier for this form
  validate, 
  asyncValidate,
  asyncBlurFields: [ 'email' ],               // <--- validation function given to redux-form
  warn                     // <--- warning function given to redux-form
})(SyncValidationForm)

export default connect(mapStateToProps, {registerUser,validateEmailExists})(SyncValidationForm);


