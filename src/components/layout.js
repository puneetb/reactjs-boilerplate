import React, { Component } from 'react';
import { Link } from 'react-router';

export default class Layout extends Component {
	render() {
		return (
			<div>
			    
				<Link to="/register">Register</Link> | <Link to="/">Home page</Link> 
				 | <Link to="/greet">Greet</Link> 
				  | <Link to="/listing">Users list</Link> 
				  | <Link to="/category/add">Add Category</Link> 
				{this.props.children}
			</div>
		);
	}
}