import ACTION_TYPES from './action_types';
import * as axios from 'axios';

const config = { headers: { 'Content-Type': 'application/json' } };


export const emailChanged = (text) => {
	console.log('Login button action received');
	return {
		type: ACTION_TYPES.EMAIL_CHANGED,
		payload: text
	}
};

export function getDueDates() {
  return dispatch => {
    console.log("IN ACTION");
    
  };
}


export const passwordChanged = (text) => {

	return {
		type: ACTION_TYPES.PASSWORD_CHANGED,
		payload: text
	}
};

export const registerUser = (userObj) => {
	console.log('Login button action received, user data' + userObj);

	return (dispatch) => {
		let responseObj = {};
		dispatch({
			type: ACTION_TYPES.SHOW_LOADER,
		});

		axios.post('http://localhost:3000/users/add', 
		{"first_name":userObj.first_name, "last_name":userObj.last_name,"email":userObj.email,"password":userObj.password}
		, config)
		  .then(function(response) {
		  	responseObj = response.data;
		  	responseObj.serverMessage = response.data.message;
		  	responseObj.errorStatus = response.data.status;
		  	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseObj
			});
		  })
		  .catch(function(error) {
		    responseObj.serverMessage = "Server error occured";
		  	responseObj.errorStatus = 0;
		  	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseObj
			});
		  })
	}


};



export const fetchUserList = () => {
	
	return (dispatch) => {
		dispatch({
			type: ACTION_TYPES.SHOW_LOADER,
		});


	  	axios.get(
			'http://localhost:3000/users/',
			null,
			{ headers: { 'Content-Type': 'application/json' } }
		)
	  	.then(function (response) {
	  		if(response.status!==200) {
	  			dispatch({
					type: ACTION_TYPES.ERROR_FETCHING
				});
	  		}
	  		
	    	dispatch({
				type: ACTION_TYPES.FETCH_USERS,
				payload: response.data.data
			});
	  	})
		.catch(function (error) {
			console.log(error);
		});


	}
}

export const deleteUser = (user_id)  => {
	console.log("inside action " +  user_id);

	return (dispatch) => {
		dispatch({
			type: ACTION_TYPES.SHOW_LOADER,
		});

		axios.delete(
			'http://localhost:3000/users/' + user_id,
			null,
			{ headers: { 'Content-Type': 'application/json' } }
		)
	  	.then(function (response) {
	  		if(response.status!==200) {
	  			dispatch({
					type: ACTION_TYPES.ERROR_FETCHING
				});
	  		}
	  		console.log("i am here");
	  		dispatch(fetchUserList());

	    	dispatch({
				type: ACTION_TYPES.DELETE_USER
			});
	  	})
		.catch(function (error) {
			console.log(error);
		});

	}
}

export const getUserDetails = (user_id) => {
	console.log("inside fetch user data function, data: " + user_id);

	return (dispatch) => {
		axios.get(
			'http://localhost:3000/users/' + user_id,
			null,
			{ headers: { 'Content-Type': 'application/json' } }
		)
	  	.then(function (response) {
	  		if(response.status!==200) {
	  			dispatch({
					type: ACTION_TYPES.ERROR_FETCHING
				});
	  		}
	  		
	    	dispatch({
				type: ACTION_TYPES.USER_DETAILS,
				payload: response.data
			});
	  	})
		.catch(function (error) {
			console.log(error);
		});
	}
}

export const updateUserData = (user_id, userObj) => {
	console.log("inside updateUserData, data: " + userObj);

	return (dispatch) => {
		let responseObj = {};
		dispatch({
			type: ACTION_TYPES.SHOW_LOADER,
		});

		axios.put('http://localhost:3000/users/update', 
		{"first_name":userObj.first_name, "last_name":userObj.last_name,"email":userObj.email,"password":userObj.password,"id":user_id,"profile_image":userObj.imageBase64}
		, config)
		  .then(function(response) {
		  	responseObj = response.data;
		  	responseObj.serverMessage = response.data.message;
		  	responseObj.errorStatus = response.data.status;
		  	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseObj
			});
		  })
		  .catch(function(error) {
		    responseObj.serverMessage = "Server error occured";
		  	responseObj.errorStatus = 0;
		  	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseObj
			});
		  })
	}
}

export const validateEmailExists = (email) => {
	console.log("inside updavalidateEmailExiststeUserData, data: " + email);

	return (dispatch) => {
		let responseObj = {};
		
		axios.post('http://localhost:3000/users/getUser', 
		{"email":email}
		, config)
		  .then(function(response) {
		  	
		   	responseObj = response.data;
		   	responseObj.serverMessage = response.data.message;
		   	responseObj.errorStatus = response.data.status;
		  	dispatch({
				type: ACTION_TYPES.CHECK_EMAIL,
				payload: responseObj
			});
		  })
		  .catch(function(error) {
		 //    responseObj.serverMessage = "Server error occured";
		 //  	responseObj.errorStatus = 0;
		 //  	dispatch({
			// 	type: ACTION_TYPES.LOGIN_USER,
			// 	payload: responseObj
			// });
		  })
	}

}
