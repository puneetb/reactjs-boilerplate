import ACTION_TYPES from './action_types';
import * as axios from 'axios';

const config = { headers: { 'Content-Type': 'application/json' } };


export const createCategory = (userObj) => {
	console.log('Login button action received, user data' + userObj);

	return (dispatch) => {
		let responseObj = {};
		dispatch({
			type: ACTION_TYPES.SHOW_LOADER,
		});

		axios.post('http://localhost:3000/users/add', 
		{"first_name":userObj.first_name, "last_name":userObj.last_name,"email":userObj.email,"password":userObj.password}
		, config)
		  .then(function(response) {
		  	responseObj = response.data;
		  	responseObj.serverMessage = response.data.message;
		  	responseObj.errorStatus = response.data.status;
		  	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseObj
			});
		  })
		  .catch(function(error) {
		    responseObj.serverMessage = "Server error occured";
		  	responseObj.errorStatus = 0;
		  	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseObj
			});
		  })
	}


};

export const fetchCategories = () => {
	console.log('fetchCategories ()');

	return (dispatch) => {
		dispatch({
			type: ACTION_TYPES.SHOW_LOADER,
		});


	  	axios.get(
			'http://localhost:3000/category/parent_cats',
			null,
			{ headers: { 'Content-Type': 'application/json' } }
		)
	  	.then(function (response) {
	  		
	  		if(response.status!==200) {
	  			dispatch({
					type: ACTION_TYPES.ERROR_FETCHING
				});
	  		}
	  		
	    	dispatch({
				type: ACTION_TYPES.FETCH_CATEGORIES,
				payload: response.data.data
			});
	  	})
		.catch(function (error) {
			console.log(error);
		});


	}


}